/*
  Themplate of the cinema hall :
  row 1 [seat1, seat2, seat3, seat4, 0-empty block, seat5, ...],
  row 2 [..., ..., ...],
  row 3 [..., ..., ...],
  ........
  row n []

  Рішення звісно не дуже елегантне, але мені здалось, 
  що візуально так буде простіше змінювати схему залу. 
  Головне, щоб кількість елементів у вкладенних масивах була однакова
  (останній ряд - дивани) 
*/

export const cinemaHall = [
  [0, 0, 1, 2, 3, 4, 0, 5, 6, 7, 8, 0, 9, 10, 11, 12, 0, 0],
  [0, 0, 1, 2, 3, 4, 0, 5, 6, 7, 8, 0, 9, 10, 11, 12, 0, 0],
  [0, 0, 1, 2, 3, 4, 0, 5, 6, 7, 8, 0, 9, 10, 11, 12, 0, 0],
  [0, 0, 1, 2, 3, 4, 0, 5, 6, 7, 8, 0, 9, 10, 11, 12, 0, 0],
  [],
  [1, 2, 3, 4, 5, 6, 7, 8, 0, 0, 9, 10, 11, 12, 13, 14, 15, 16],
  [1, 2, 3, 4, 5, 6, 7, 8, 0, 0, 9, 10, 11, 12, 13, 14, 15, 16],
  [1, 2, 3, 4, 5, 6, 7, 8, 0, 0, 9, 10, 11, 12, 13, 14, 15, 16],
  [1, 2, 3, 4, 5, 6, 7, 8, 0, 0, 9, 10, 11, 12, 13, 14, 15, 16],
  [1, 2, 3, 4, 5, 6, 7, 8, 0, 0, 9, 10, 11, 12, 13, 14, 15, 16],
  [],
  [1, 0, 2, 0, 3, 0, 0, 4, 0, 5, 0, 6]
];

// ticket prices
export const low = 60;
export const medium = 90;
export const high = 200;