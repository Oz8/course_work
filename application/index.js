import {cinemaHall, low, medium, high} from './modules/cinema-themplate.js';
import './assets/style.scss';

// empty cell
class Empty {

  constructor(type, width, row) {
    this.type = type;
    this.cellWidth = 100 / width;
    this.row = row; 
    this.render();
  }

  render() {
    let body = document.getElementById('hall');
    let rowCover = document.createElement('div');
    rowCover.setAttribute('style', 'width: ' + this.cellWidth + '%;');
    rowCover.innerHTML = `<span class="row-cell">${this.row}</span>`;
    rowCover.classList.add('cell');
    body.appendChild(rowCover);
  }
}

// seats
class Seat {

  constructor(type, row, seat, price, width, booked) {
    this.type = type;
    this.row = row;
    this.seat = seat;
    this.price = price;
    this.cellWidth = 100 / width; 
    this.booked = booked;
    this.selected = false;
    this.selectedSeat = this.selectedSeat.bind(this);  
    this.ticketInBox;
    
    this.render();
  }

  render() {
    let body = document.getElementById('hall');
    let rowCover = document.createElement('div');

    // if type is high - make width of the cell longer in 2 times 
    if (this.type === 'high') {
      rowCover.classList.add('cell-high');
      rowCover.setAttribute('style', 'width: ' + this.cellWidth * 2 + '%;');
    } else {
      // regular cell
      rowCover.classList.add('cell');
      rowCover.setAttribute('style', 'width: ' + this.cellWidth + '%;');
    }

    // if it's booked - make cell not active 
    if (this.booked) {
      rowCover.innerHTML = `<div class="not-active"></div>`;
    } else {
      rowCover.innerHTML = `<div id="${this.row}-${this.seat}" class="${this.type}">${this.seat}</div>`;
      rowCover.addEventListener('click', this.selectedSeat);
    }

    body.appendChild(rowCover);
  }

  selectedSeat() {
    // id of the seat is its row - seat, eg 1-10
    let id = this.row + '-' + this.seat; 
    let currentSeat = document.getElementById(id);
    
    // on first click add class 'selected' 
    if (!this.selected) {
      currentSeat.classList.add('selected');    
      this.ticketBox(currentSeat);
      this.selected = true;
      total(this.price, '+');
    } else {
      // remove class 'selected' and remove it from the tickts list 
      // make it available for booking
      currentSeat.classList.remove('selected');
      this.selected = false;
      total(this.price, '-');
      this.ticketInBox.parentNode.removeChild(this.ticketInBox); 
    }
    
    // run count down
    if (!countDownStatus) {
      waitingCounter();
      countDownStatus = true;
    }
  }

  ticketBox(currentSeat) {
    // show selected ticket list and slide cinema hall to the left
    let box = document.getElementById('ticket-box');
    let ticketsCover = document.querySelector('.tickets-cover');
    let hall = document.getElementById('hall');
    let screen = document.querySelector('.screen');
    box.classList.add('active');
    hall.classList.add('slide-left');
    screen.classList.add('slide-left');

    let ticket = document.createElement('div');
    ticket.classList.add('ticket');

    // add a ticket to the ticket list
    ticket.innerHTML = `
      <p>Ряд: ${this.row}; Місце: ${this.seat}</p>
      <p>Ціна: <span class="ticket-price">${this.price}</span></p>
      <button class="remove-ticket">&times;</button>`;

    ticketsCover.appendChild(ticket);

    // remove ticket from the list
    let removeBtn = ticket.querySelector('.remove-ticket');
    removeBtn.addEventListener('click', removeTicket);
    this.ticketInBox = ticket;

    function removeTicket() {
      let price = ticket.getElementsByTagName('p')[1].querySelector('.ticket-price').textContent;
      total(price, '-');
      currentSeat.classList.remove('selected');
      this.selected = false;
      ticket.parentNode.removeChild(ticket);      
    }
  }
}

let currentRow = 0;


for (let row = 0; row < cinemaHall.length; row++) {
  
  // print row from number 1 (instead of 0)
  let hallWidth = cinemaHall[0].length + 1;

  // if row length less then 1 - it's empty row
  // make 1 cell with width: 100%  
  if(cinemaHall[row].length < 1) {
    let emptyCell = new Empty('empty', 1, '');
    continue;
  } else {
    // regular empty cells
    currentRow++;
    let emptyCell = new Empty('empty', hallWidth, currentRow);
  }

  // print seats
  for (let seat = 0; seat < cinemaHall[row].length; seat++) {

    if(cinemaHall[row][seat] === 0) {
      // if array[n] === 0 create empty cell  
      let emptyCell = new Empty('empty', hallWidth, '');
    } else {
      let type = '';
      let price = 0;

      // add price according to a type of seat
      if ([row] <= 2) {
        type = 'low';
        price = low;
      } else if ([row] < cinemaHall[row].length -1) {
        type = 'medium';
        price = medium;
      } else {
        type = 'high';
        price = high;
      }

      // check local storage for booked seats
      let seatId = currentRow + '-' + cinemaHall[row][seat];
      let seatInLocalStorage = localStorage.getItem(seatId);
      let booked;

      if (seatInLocalStorage) {
        booked = true;
      } else {
        booked = false;
      }

      let cell = new Seat(type, currentRow, cinemaHall[row][seat], price, hallWidth, booked);
    }
  }
  
}

// count total price
let orderButton = document.querySelector('#order span');
let totalPrice = 0;
function total(price, sign) {
  if (sign === '+') {
    totalPrice += price;
  } else {
    totalPrice -= price;
  }
  orderButton.innerHTML = totalPrice; 
} 

// countdown 
let countDownStatus = false;

function waitingCounter() {
  
  let message = document.querySelector('.counter-message');
  let counter = document.getElementById('countdown');
  let sec = 60;
  counter.innerHTML = sec;

  let conter = setInterval(function() {
    if (sec > 0) {
      sec--;
      counter.innerHTML = sec;
    } else {
      clearInterval(conter);
      message.innerHTML = '';
      message.innerHTML = 'Час вийшов';
      
      setTimeout(function() {
        location.reload();          
      }, 3000); 
    }
  }, 1000);
}

// book a tickets

let orderBtn = document.getElementById('order');
let hall = document.getElementById('hall');
let selectedSeats = hall.getElementsByClassName('selected');
let popup = document.getElementById('popup');
let reloadBtn = document.getElementById('reload');

// add tickes to localStorage by clicking on order button
orderBtn.addEventListener('click', function() {
  for (let i = 0; i < selectedSeats.length; i++) {
    localStorage.setItem(selectedSeats[i].id, 'booked');
  }
  popup.classList.add('active');
});

reloadBtn.addEventListener('click', function() {
  location.reload(); 
});