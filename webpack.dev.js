
const path = require('path');
const common = require('./webpack.common');
const marge = require('webpack-merge');

module.exports = marge(common, {
  output: {
      path: path.resolve(__dirname, 'public'),
      filename: 'bundle.js'
  },
  mode: 'development',
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader'
        ]
      },
    ]
  }
});
